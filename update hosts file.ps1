#Connect to vcenter


#du må installere Posh-SSH 

$vcenter = 'vcenter.local'
connect-viserver -server $vcenter
# her kan du ha en eller flerer hoster i $hosts, root pw må være samme 
$hosts = get-vmhost -Name host.locale
# om du vil importere hostene fra en TXT file
$esxName = Get-Content -Path "C:\Path\to\textfilewithhostnames.txt"
#brukernavn og passord for esxi host, det må være lik 
$cred = Get-Credential -Message 'Enter credentials'
---
Part 1
foreach($esxNames in $hosts)
{
$cmdsub = @'
cp /etc/hosts /etc/hosts.old
echo "10.10.10.10    newhost.local newhost" >> /etc/hosts
'@
$esx = Get-VMHost -Name $esxNames
$session = New-SSHSession -ComputerName $esx.Name -Credential $cred –AcceptKey
$result = Invoke-SSHCommand -SSHSession $session -Command $cmdsub
Remove-SSHSession -SSHSession $session | Out-Null
}
-----
Part 2
foreach($esxNames in $hosts)
{
$cmdsub = @'
rm  /etc/hosts
mv  /etc/hosts.old /etc/hosts
'@
$esx = Get-VMHost -Name $esxNames
$session = New-SSHSession -ComputerName $esx.Name -Credential $cred –AcceptKey
$result = Invoke-SSHCommand -SSHSession $session -Command $cmdsub
Remove-SSHSession -SSHSession $session | Out-Null
}
